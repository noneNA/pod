# Pod

Pod is an alternative launcher for [Risk of Rain Modloader](https://rainfusion.ml/). It was made for Linux support, though Windows and macOS are also possible targets.

## Downloads

The pre-built executable binaries can be found on the [releases page](https://gitlab.com/noneNA/pod/-/releases).

## Compiling

For all platforms the resulting executable should be in `build/`  
All commands in this section should be run on the project root where the `README.md` file is  
Make sure you also get the submodules with this command first:

```bash
git submodule update --init --recursive
```

### Build Dependencies

* cmake
* make (or a make alternative)
* g++ (gcc) (or other c++ compiler)
* zlib (libz)
* git
* qt5

### Runtime Dependencies

* qt5
* wine (or proton) (for running RoRML on platforms other than Windows)

### CMake

```bash
./cmake_build.sh
```

The regular CMake build process can be used without the script as well.

### Nix

[Nix](https://nixos.org/) can also be used to build. The executable will be in `./result/bin/pod`

```bash
./nix_build_flake.sh
```

### macOS

See `macOS.md` for additional information.

## Notes

### Setting Up
This launcher needs the Windows version of Risk of Rain to work.  
To get it you can either:
* Use [SteamCMD](https://developer.valvesoftware.com/wiki/SteamCMD).
* Force Proton/Wine (Steam Play) from Steam (if you have Risk of Rain on Steam).
	* Steam Library
	* Right click `Risk of Rain`
	* Properties
	* Compatibility
	* Enable `Force the use of a specific Steam Play compatibility tool`

### Saves
Saves are not imported by default. Therefore progress is not saved until a new save is created or a vanilla save is imported. Both can be done under the Profiles tab in the launcher.

## Contact

If you have a problem or would like to discuss something:
* Email [nonena@protonmail.com](mailto:nonena@protonmail.com)
* Discord none#3549
* [Risk of Rain 1 Discord Server](https://discord.com/invite/ajsGTdN)
