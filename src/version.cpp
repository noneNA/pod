#include <version.h>

#include <sstream>

namespace version {
	static int const MAX_NUMBER = 3;

	SemanticVersion::SemanticVersion() {}

	SemanticVersion::SemanticVersion(std::string version_string) {
		fromString(version_string);
	}

	bool SemanticVersion::isValid() const {
		return numbers.size() > 0;
	}

	COMPARE SemanticVersion::compare(IVersion const * other) const {
		SemanticVersion const * version = dynamic_cast<SemanticVersion const *>(other);
		if (!version)
			return NOCOMPARE;

		int this_size = numbers.size();
		int that_size = version->numbers.size();
		int count = this_size > that_size ? that_size : this_size;
		for (int i = 0; i < count; i++) {
			if (numbers[i] > version->numbers[i]) {
				return GREATER;
			}
			else if (numbers[i] < version->numbers[i]) {
				return LESSER;
			}
		}
		if (this_size > that_size) {
			for (int i = that_size; i < this_size; i++) {
				if (numbers[i] > 0) {
					return GREATER;
				}
			}
			return EQUAL;
		}
		else if (this_size < that_size) {
			for (int i = this_size; i < that_size; i++) {
				if (version->numbers[i] > 0) {
					return LESSER;
				}
			}
			return EQUAL;
		}
		else {
			return EQUAL;
		}
	}

	std::string SemanticVersion::toString() const {
		std::string version_string;
		for (int i = 0; i < numbers.size(); i++) {
			if (i)
				version_string += ".";
			version_string += std::to_string(numbers[i]);
		}
		return version_string;
	}

	void SemanticVersion::fromString(std::string version_string) {
		numbers.clear();
		std::istringstream version_stream(version_string);
		int number;
		while (true) {
			version_stream >> number;
			numbers.push_back(number);
			if ((version_stream.get() == -1) || (numbers.size() >= MAX_NUMBER)) {
				break;
			}
		}
	}
}
