#ifndef RUNNER_INFO_H
#define RUNNER_INFO_H

#include <QMetaType>

#include <string>
#include <vector>

enum struct runner_type {
	Wine,
	Proton,

	// meta runners
	Native,
	Missing,
};

struct runner_info {
	runner_type type;
	std::string short_name;
	std::string path;

	static std::vector<runner_info> getRunners();
	static std::vector<runner_info> getWineRunners();
	static std::vector<runner_info> getProtonRunners();
};

Q_DECLARE_METATYPE(runner_info)

#endif //RUNNER_INFO_H
