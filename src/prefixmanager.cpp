#include <prefixmanager.h>

#include <links.h>
#include <data.h>
#include <vanilla.h>
#include <prefix.h>
#include <global.h>

#include <thread>

#include <platform_quazip.h>

#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QBoxLayout>

PrefixManager::PrefixManager() {
	netman = new QNetworkAccessManager;
}

PrefixManager::~PrefixManager() {
	netman->deleteLater();
}

bool PrefixManager::valid() {
	return validVanilla();
}

bool PrefixManager::validVanilla() {
	return vanilla::isVanillaDirectory(d_vanilla.toStdString());
}

QString PrefixManager::getVanillaDirectory() {
	return d_vanilla;
}

bool PrefixManager::setVanillaDirectory(QString d_vanilla) {
	QString d_clean = util::getDirectory(d_vanilla);
	if (!vanilla::isVanillaDirectory(d_vanilla.toStdString()))
		return false;

	global::settings->setValue("data_win", d_clean + "/data.win");
	this->d_vanilla = d_vanilla;
	emit changedVanilla();
	return true;
}

bool PrefixManager::detectVanillaDirectory() {
	if (vanilla::isVanillaDirectory(d_vanilla.toStdString()))
		return true;

	if (vanilla::isVanillaDirectory(global::d_application.toStdString())) {
		setVanillaDirectory(global::d_application);
		return true;
	}

	return false;
}

bool PrefixManager::grabVanillaDirectory(QWidget * widget) {
	QString filename;
	while (true) {
		filename = QFileDialog::getOpenFileName(
			widget,
			"Open the Vanilla 'Risk of Rain.exe'",
			"",
			"Executables (*.exe)"
		);
		
		if (filename == "") {
			qInfo() << "Cancel selection";
			return false;
		}
		
		std::string d_given = util::getDirectory(filename).toStdString();
		if (vanilla::isVanillaDirectory(d_given)) {
			setVanillaDirectory(QString::fromStdString(d_given));
			return true;
		}
		else {
			QMessageBox::information(widget, "Setup", "Selected file wasn't a vanilla install");
		}
	}

	return false;
}

bool PrefixManager::installUrl(QUrl url, QString d_prefix, PrefixUpdateInfo * info) {
	// TODO Check if updating instead of installing and let that pass this check
	if (!valid())
		return false;

	PrefixUpdateInfo updateinfo;
	if (info) {
		updateinfo = *info;
	} else {
		updateinfo.d_prefix = d_prefix;
		TaskInfo * taskinfo = new TaskInfo;
		taskinfo->setProgress(0);
		taskinfo->setState(RUNNING);
		updateinfo.task = taskinfo;
		emit taskAdded(taskinfo);
	}
	updateinfo.task->setText("Downloading modloader");

	QNetworkReply * reply = netman->get(QNetworkRequest(url));
	connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error), this, &PrefixManager::downloadModloaderError);
	connect(reply, &QNetworkReply::finished, this, &PrefixManager::downloadModloaderFinished);
	connect(reply, &QNetworkReply::downloadProgress, this, &PrefixManager::downloadModloaderProgress);

	ongoing_updates[reply] = updateinfo;
	return true;
}

bool PrefixManager::installLink(QString link, QString d_prefix, PrefixUpdateInfo * info) {
	return installUrl(QUrl(link), d_prefix, info);
}

bool PrefixManager::installRainfusion(QString d_prefix, bool beta, bool skip_version, QWidget * widget) {
	QString info_link = beta ? links::BETA_INFO : links::GAME_INFO;

	QNetworkReply * reply = netman->get(QNetworkRequest(QUrl(info_link)));
	connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error), this, &PrefixManager::downloadRainfusionError);
	connect(reply, &QNetworkReply::finished, this, &PrefixManager::downloadRainfusionFinished);
	connect(reply, &QNetworkReply::downloadProgress, this, &PrefixManager::downloadRainfusionProgress);

	PrefixUpdateInfo updateinfo;
	updateinfo.d_prefix = d_prefix;
	updateinfo.skip_version = skip_version;
	updateinfo.widget = widget;

	TaskInfo * taskinfo = new TaskInfo;
	taskinfo->setProgress(0);
	taskinfo->setText("Fetching rainfusion version info");
	taskinfo->setState(RUNNING);
	updateinfo.task = taskinfo;
	emit taskAdded(taskinfo);

	ongoing_updates[reply] = updateinfo;

	qInfo() << "Fetching rainfusion version info";
	return true;
}

void PrefixManager::downloadRainfusionProgress(qint64 received, qint64 total) {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply || !ongoing_updates.count(reply)) {
		return;
	}
	TaskInfo * taskinfo = ongoing_updates.at(reply).task;
	int progress = (int)((double)received * 100 / total);
	taskinfo->setProgress(progress);
}

void PrefixManager::cancelDownload(QNetworkReply * reply) {
	if (ongoing_updates.find(reply) != ongoing_updates.end()) {
		PrefixUpdateInfo info = ongoing_updates.at(reply);
		if (info.task) {
			info.task->setProgress(100);
			info.task->setState(FAILURE);
			info.task = nullptr;
		}
		ongoing_updates.erase(ongoing_updates.find(reply));
	}
	reply->deleteLater();
}

void PrefixManager::downloadRainfusionError(QNetworkReply::NetworkError code) {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply || !ongoing_updates.count(reply)) {
		return;
	}
	PrefixUpdateInfo updateinfo = ongoing_updates.at(reply);
	updateinfo.task->setText("Failed to download modloader information from rainfusion");
	cancelDownload(reply);
}

void PrefixManager::downloadRainfusionFinished() {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply)
		return;

	if (reply->error()) {
		cancelDownload(reply);
		return;
	}

	PrefixUpdateInfo updateinfo = ongoing_updates.at(reply);

	std::string localversion = prefix::getPrefixVersionString(updateinfo.d_prefix.toStdString());

	std::string remoteversion = QString(reply->readLine()).remove('\n').remove('\r').toStdString();
	QString download = reply->readLine();

	qInfo() << "Got version " << QString::fromStdString(remoteversion);

	bool new_version = localversion != remoteversion;
	bool update = prefix::isPrefixDirectory(updateinfo.d_prefix.toStdString());
	bool confirm = new_version || updateinfo.skip_version;

	// TODO
	if (!update && !valid()) {
		updateinfo.task->setText("Install failed since vanilla isn't valid");
		cancelDownload(reply);
		return;
	}

	if (updateinfo.widget && update) {
		if (new_version) {
			auto answer = QMessageBox::question(updateinfo.widget, "Update Prefix", QString("New Version found, would you like to update (%1->%2)?").arg(QString::fromStdString(localversion), QString::fromStdString(remoteversion)));
			if (answer == QMessageBox::Yes) {
				confirm = true;
				QMessageBox::information(updateinfo.widget, "Update Prefix", "Update task has been added");
			}
			else {
				confirm = false;
			}
		} else {
			QMessageBox::information(updateinfo.widget, "Update Prefix", QString("Prefix %1 is the latest version available (%2)").arg(updateinfo.d_prefix, QString::fromStdString(localversion)));
			updateinfo.task->setProgress(100);
			updateinfo.task->setText("Checked prefix version");
			updateinfo.task->setState(SUCCESS);
			updateinfo.task = nullptr;
		}
	}

	if (confirm) {
		qInfo() << "Install confirmed";
		installLink(download, updateinfo.d_prefix, &updateinfo);
	} else {
		PrefixUpdateInfo p = ongoing_updates.at(reply);
		p.task->setText("Version check completed");
		p.task->setProgress(100);
		p.task->setState(SUCCESS);
		emit changedPrefixes();
	}

	ongoing_updates.erase(ongoing_updates.find(reply));
	reply->deleteLater();
}

void PrefixManager::downloadModloaderFinished() {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply)
		return;

	if (!reply->error()) {
		PrefixUpdateInfo info = ongoing_updates.at(reply);
		QBuffer * buffer = new QBuffer;
		buffer->setData(reply->readAll());
		std::thread t(&PrefixManager::installBuffer, this, buffer, info.d_prefix, info.task);
		t.detach();
		ongoing_updates.erase(ongoing_updates.find(reply));
		reply->deleteLater();
	}
	else {
		cancelDownload(reply);
		return;
	}
}

void PrefixManager::downloadModloaderError(QNetworkReply::NetworkError code) {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply || !ongoing_updates.count(reply)) {
		return;
	}
	PrefixUpdateInfo updateinfo = ongoing_updates.at(reply);
	updateinfo.task->setText("Failed to download modloader from rainfusion");
	cancelDownload(reply);
}

void PrefixManager::downloadModloaderProgress(qint64 received, qint64 total) {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply || !ongoing_updates.count(reply))
		return;
	TaskInfo * taskinfo = ongoing_updates.at(reply).task;
	int progress = (int)((double)received * 40 / total);
	taskinfo->setProgress(progress);
}

// TODO Make sure this doesn't touch anything not locked
bool PrefixManager::installBuffer(QBuffer * modloader, QString d_prefix, TaskInfo * taskinfo) {
	taskinfo->setText("Extracting modloader");
	taskinfo->setProgress(50);

	QString path = d_prefix;
	qInfo() << "Extracting to " << path;
	if (!QDir().mkpath(path))
		return false;
	JlCompress::extractDir(modloader, path);

	taskinfo->setProgress(60);
	taskinfo->setText("Extracting vanilla resources");

	// TODO Lock
	QString data_filename = d_vanilla + "/data.win";
	Data * extractor = new Data;
	std::thread extraction(&Data::extractResources, extractor, data_filename, path);
	while (true) {
		extractor->lock.lock();
		if (extractor->completed)
			break;
		int progress = (int)((double)extractor->progress * 35 / FILE_COUNT) + 1;
		extractor->lock.unlock();

		// TODO For some reason without this line taskinfo locks the main thread
		// Even after everything finished
		QThread::msleep(100);

		taskinfo->setProgress(60 + progress);
	}
	extraction.join();
	delete extractor;
	
	taskinfo->setProgress(95);
	taskinfo->setText("Creating file structure");
	
	if (!QDir().mkpath(d_prefix + "/mods"))
		return false;
	if (!QDir().mkpath(d_prefix + "/profiles/Default"))
		return false;

	QFile profile(d_prefix + "/profiles/Default/profile.json");
	if (!profile.open(QIODevice::WriteOnly))
		return false;

	QJsonObject json;
	QJsonArray mods;
	json["mods"] = mods;

	QJsonDocument document(json);
	profile.write(document.toJson());

	QSettings launcher_settings(d_prefix + "/launchersettings.ini", QSettings::IniFormat);
	launcher_settings.beginGroup("launcher");
	launcher_settings.setValue("profile", "Default");
	launcher_settings.endGroup();

	taskinfo->setText("Completed setup");
	taskinfo->setProgress(100);
	taskinfo->setState(SUCCESS);
	qInfo() << "Setup completed";

	addPrefix(prefix::Prefix(d_prefix.toStdString()));
	emit changedPrefixes();
	return true;
}

std::vector<prefix::Prefix> PrefixManager::getPrefixes() {
	return prefixes;
}

bool PrefixManager::addPrefix(prefix::Prefix entry) {
	// TODO
	for (prefix::Prefix & prefix : prefixes)
		if (prefix.path == entry.path)
			return false;
	prefixes.push_back(entry);
	return true;
}

void PrefixManager::detectPrefixes() {
	QList<QString> search_dirs;

	if (validVanilla())
		search_dirs.push_back(getVanillaDirectory());

	search_dirs.push_back(global::getApplicationDirectory());

	for (QString & dir : search_dirs) {
		QStringList prefix_dirs = QDir(dir).entryList(QStringList(), QDir::Dirs);
		for (QString & prefix_dir : prefix_dirs) {
			if (prefix_dir == "." || prefix_dir == "..")
				continue;
			prefix_dir = dir + "/" + prefix_dir;
			if (prefix::isPrefixDirectory(prefix_dir.toStdString())) {
				addPrefix(prefix::Prefix(prefix_dir.toStdString()));
			}
		}
	}

	std::string d_app = global::getApplicationDirectory().toStdString();
	if (prefix::isPrefixDirectory(d_app))
		addPrefix(prefix::Prefix(d_app));
}
