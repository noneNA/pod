#ifndef MOD_H
#define MOD_H

#include <version.h>

#include <vector>
#include <string>

namespace mod {
	bool isModDirectory(std::string directory);

	enum INFO {
		ID,
		INTERNALNAME,
		NAME,
		VERSION,
		AUTHOR,
		TYPE,
		SUMMARY,
		DESCRIPTION
	};

	enum TEXT_TYPE {
		PLAIN,
		HTML,
		MARKDOWN
	};

	class IMod {
		public:
			virtual std::string getInfo(INFO /* info_type */) const { return "\0"; }
			virtual TEXT_TYPE getTextType(INFO /* info_type */) const { return PLAIN; }
	};

	class Mod : public IMod {
		public:
			std::string getInfo(INFO info_type) const override;

			std::string internalname;
			std::string name;
			version::SemanticVersion version;
			std::string author;
			std::string description;
			std::vector<Mod> dependencies;
	};

	class ModPath : public Mod {
		public:
			ModPath();
			ModPath(std::string path);

			bool valid() const;
			bool refresh();
			bool read();
			bool setPath(std::string path);

			std::string path;
	};

	ModPath getMod(std::string d_mod);
}

#endif //MOD_H
