#include <launcher.h>

#include <global.h>
#include <vanilla.h>

#include <algorithm>

#include <QDesktopWidget>
#include <QBoxLayout>
#include <QMessageBox>

void Launcher::launch() {
	if (!current_prefix.valid()) {
		QMessageBox::information(this, "Launcher", "Current prefix isn't valid.");
		button_launch->setEnabled(false);
		return;
	} else if (this->selected_runner.type == runner_type::Missing) {
		QMessageBox::information(this, "Launcher", "No valid runner available.");
		button_launch->setEnabled(false);
		return;
	}

	QProcess * process = new QProcess(this);

	process->setWorkingDirectory(QString::fromStdString(current_prefix.path));
	QString rorml_exe_path = QString::fromStdString(current_prefix.path + "/RoRML.exe");

	switch (this->selected_runner.type) {
		// to suppress warning
		case runner_type::Missing:
			break;

		case runner_type::Native:
			process->setProgram(rorml_exe_path);
			break;

		case runner_type::Wine:
			process->setArguments(QStringList() << rorml_exe_path);
			process->setProgram(QString::fromStdString(this->selected_runner.path));
			break;

		case runner_type::Proton:
			QProcessEnvironment env = QProcessEnvironment::systemEnvironment();

			env.insert(
				"STEAM_COMPAT_CLIENT_INSTALL_PATH",
				QDir::home().absoluteFilePath(".local/share/Steam")
			);

			auto compat_dir = QDir(QString::fromStdString(current_prefix.path));
			compat_dir.mkdir("compatdata");
			compat_dir.cd("compatdata");

			env.insert("STEAM_COMPAT_DATA_PATH", compat_dir.absolutePath());
			env.insert("WINEPREFIX", compat_dir.absoluteFilePath("pfx"));

			process->setProcessEnvironment(env);
			process->setArguments(QStringList() << "run" << rorml_exe_path);
			process->setProgram(QString::fromStdString(this->selected_runner.path));
			break;
	}

	process->start();
	if (!process->waitForStarted()) {
		QMessageBox::information(this, "Launcher", "Couldn't launch process");
	}
	else {
		launched_instances.push_back(process);

		connect(process, &QProcess::readyReadStandardOutput, [this, process] () {
			if (!process) {
				return;
			}
			logs->log(process->readAllStandardOutput());
		});
		connect(process, &QProcess::readyReadStandardError, [this, process] () {
			if (!process) {
				return;
			}
			logs->log(process->readAllStandardError());
		});

		connect(
			process,
			QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
			[this, process] (int, QProcess::ExitStatus) {
				if (!(this && process)) {
					return;
				}
				// https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom
				launched_instances.erase(
					std::remove(
						launched_instances.begin(),
						launched_instances.end(),
						process
					),
					launched_instances.end()
				);
				refreshLaunched();
			}
		);
	}

	refreshLaunched();
}

Launcher::Launcher(QWidget * parent) : QMainWindow(parent) {
	prefixmanager = new PrefixManager();
	connect(prefixmanager, &PrefixManager::taskAdded, this, &Launcher::taskAdded);
	connect(prefixmanager, &PrefixManager::taskAdded, [this] (TaskInfo * task) {
		connect(task, &TaskInfo::completed, this, &Launcher::refresh);
	});
	connect(prefixmanager, &PrefixManager::changedPrefixes, this, &Launcher::refresh);
	connect(prefixmanager, &PrefixManager::changedVanilla, this, &Launcher::refreshVanilla);

	setWindowTitle("Pod");
	setWindowIcon(QIcon(":/icon.ico"));

	QVBoxLayout * layout_main = new QVBoxLayout;
	QTabWidget * tabs = new QTabWidget;
	layout_main->addWidget(tabs);

	QHBoxLayout * layout_launch = new QHBoxLayout;

	combo_profile = new QComboBox;
	connect(combo_profile, QOverload<int>::of(&QComboBox::activated), this, &Launcher::selectedProfile);

	label_launched = new QLabel("Not running");
	//label_launched->setAlignment(Qt::AlignCenter);
	layout_launch->addWidget(label_launched);

	// TODO refresh ability
	auto runners = runner_info::getRunners();
	this->selected_runner = runners.at(0);

	auto only_native = runners.size() == 1 && runners.at(0).type == runner_type::Native;
	if (!only_native) {
		combo_runner = new QComboBox;
		connect(combo_runner, QOverload<int>::of(&QComboBox::activated), this, &Launcher::selectedRunner);
		layout_launch->addWidget(combo_runner);

		combo_runner->setEnabled(this->selected_runner.type != runner_type::Missing);

		for (auto const & runner : runners) {
			QVariant v;
			v.setValue(runner);
			combo_runner->addItem(QString::fromStdString(runner.short_name), v);
		}
	}

	layout_launch->addWidget(combo_profile);

	button_launch = new QPushButton("Launch");
	connect(button_launch, &QPushButton::clicked, this, &Launcher::launch);
	layout_launch->addWidget(button_launch);

	layout_main->addLayout(layout_launch);
	QWidget * central = new QWidget();
	central->setLayout(layout_main);
	setCentralWidget(central);

	// Window size
	QDesktopWidget desktop;
	QRect available = desktop.availableGeometry(this);
	int w = available.width(); int h = available.height();
	resize(w * 0.4, h * 0.4);
	move(w * (0.5 - 0.4/2), h * (0.5 - 0.4/2));

	QString settings_vanilla = global::settings->value("data_win").toString();
	QString settings_d_vanilla = util::getDirectory(settings_vanilla);
	if (vanilla::isVanillaDirectory(settings_d_vanilla.toStdString()))
		prefixmanager->setVanillaDirectory(settings_d_vanilla);

	// TODO
	current_prefix = prefix::Prefix(prefixmanager->getVanillaDirectory().toStdString() + "/ModLoader");
	current_prefix.refresh();

	if (current_prefix.valid())
		// TODO
		prefixmanager->addPrefix(current_prefix);
	prefixmanager->detectPrefixes();

	if (current_prefix.valid())
		qInfo() << "Prefix is " + QString::fromStdString(current_prefix.path);
	else
		qInfo() << "No prefix";

	profile::ProfileFile current_profile = prefix::getCurrentProfile(current_prefix.path);

	home = new tab_home();
	tabs->addTab(home, "Home");

	tasks = new tab_tasks();
	tabs->addTab(tasks, "Tasks");

	mods = new tab_mods();
	mods->setPrefix(current_prefix);
	tabs->addTab(mods, "Mods");

	profiles = new tab_profile();
	profiles->setPrefix(current_prefix);
	profiles->setProfile(current_profile);
	connect(profiles, &tab_profile::changedCurrentProfile, this, &Launcher::changedCurrentProfile);
	connect(profiles, &tab_profile::changedProfiles, this, &Launcher::changedProfiles);
	tabs->addTab(profiles, "Profiles");

	sources = new tab_sources();
	sources->setPrefix(current_prefix);
	connect(sources, &tab_sources::taskAdded, this, &Launcher::taskAdded);
	connect(sources, &tab_sources::changedMods, mods, &tab_mods::refreshMods);
	tabs->addTab(sources, "Sources");

	prefix = new tab_prefix();
	prefix->setPrefixManager(prefixmanager);
	prefix->setPrefix(current_prefix);
	connect(prefix, &tab_prefix::changedCurrentPrefix, this, &Launcher::changedCurrentPrefix);
	tabs->addTab(prefix, "Prefixes");

	logs = new tab_logs();
	tabs->addTab(logs, "Logs");

	// This makes the window obey the window manager's focus policy a bit better
	setAttribute(Qt::WA_ShowWithoutActivating);

	button_launch->setEnabled(false);

	// First time
	// TODO
	if (!(vanilla::isVanillaDirectory(prefixmanager->getVanillaDirectory().toStdString()) && current_prefix.valid())) {
		auto answer = QMessageBox::question(this, "Pod Setup", "Setup now? (Advanced setup can be done later from the launcer)", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
		if (answer == QMessageBox::Yes) {
			if (!prefixmanager->detectVanillaDirectory())
				prefixmanager->grabVanillaDirectory(this);
			if (prefixmanager->valid()) {
				prefixmanager->installRainfusion(prefixmanager->getVanillaDirectory() + "/ModLoader", false, false, this);
			}
		}
	}

	profiles->d_vanilla = prefixmanager->getVanillaDirectory().toStdString();

	refresh();

	tabs->setCurrentWidget(tasks);
}

Launcher::~Launcher() {
	delete prefixmanager;
}

void Launcher::refresh() {
	if (!current_prefix.valid()) {
		prefixmanager->detectPrefixes();
		std::vector<prefix::Prefix> prefixes = prefixmanager->getPrefixes();
		if (prefixes.size() < 1) // No prefixes
			return; // TODO
		current_prefix = prefixes.front(); // TODO Pick only valid
	}

	QString d_prefix = QString::fromStdString(current_prefix.path);
	profile::ProfileFile profile = prefix::getCurrentProfile(d_prefix.toStdString());

	profile.read();

	//current_prefix.setPath(d_prefix.toStdString());

	changedProfiles();
	changedCurrentProfile(current_prefix.getCurrentProfile());

	// TODO
	mods->setPrefix(current_prefix);
	mods->setProfile(profile);
	profiles->setPrefix(current_prefix);
	profiles->setProfile(profile);
	prefix->refreshPrefixes();
	prefix->setPrefix(current_prefix);
	sources->setPrefix(current_prefix);
	home->changedCurrentProfile(profile);
	home->refresh();

	button_launch->setEnabled(
		current_prefix.valid()
		&& this->selected_runner.type != runner_type::Missing
	);
}

void Launcher::refreshLaunched() {
	int instances = launched_instances.size();
	QString text;
	if (instances == 0)
		text = "No running instances";
	else
		text = "Instances: " + QString::number(instances);
	label_launched->setText(text);
}

void Launcher::changedCurrentPrefix(QString d_prefix) {
	current_prefix = prefix::Prefix(d_prefix.toStdString());

	mods->setPrefix(current_prefix);
	profiles->setPrefix(current_prefix);
	sources->setPrefix(current_prefix);
	prefix->setPrefix(current_prefix);

	refresh();
}

void Launcher::changedCurrentProfile(profile::ProfileFile profile) {
	mods->setProfile(profile);
	mods->refreshMods();

	profiles->setProfile(profile);

	int index = combo_profile->findText(profile.getName());
	if (index != -1)
		combo_profile->setCurrentIndex(index);

	// TODO
	prefix::setCurrentProfile(current_prefix.path, profile.getName().toStdString());

	// TODO
	home->changedCurrentProfile(profile);
}

void Launcher::selectedProfile(int index) {
	QString profile = combo_profile->itemText(index);
	profile::ProfileFile p = prefix::getProfile(current_prefix.path, profile.toStdString());
	emit changedCurrentProfile(p);
}

void Launcher::selectedRunner(int index) {
	this->selected_runner = combo_runner->itemData(index).value<runner_info>();
}

void Launcher::taskAdded(TaskInfo * info) {
	tasks->model->addTask(info);
}

void Launcher::changedProfiles() {
	while (combo_profile->count() > 0)
		combo_profile->removeItem(0);

	current_prefix.refresh();
	std::vector<profile::ProfileFile> p = current_prefix.profiles;
	QStringList items;
	for (size_t i = 0; i < p.size(); i++) {
		items << p.at(i).getName();
	}
	combo_profile->addItems(items);

	changedCurrentProfile(current_prefix.getCurrentProfile());
}

void Launcher::refreshVanilla() {
	if (prefix)
		prefix->refreshVanilla();
}
