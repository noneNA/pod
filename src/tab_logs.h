#ifndef TAB_LOGS
#define TAB_LOGS

#include <QWidget>
#include <QTextEdit>

class tab_logs : public QWidget {
	Q_OBJECT

	public:
		tab_logs();

		void log(QString text);

	private:
		QTextEdit * view_logs = nullptr;
};

#endif //TAB_LOGS
