#ifndef TAB_SOURCES_H
#define TAB_SOURCES_H

#include <modview.h>
#include <taskmodel.h>
#include <prefix.h>

#include <map>
#include <unordered_map>

#include <QWidget>
#include <QListView>
#include <QTreeView>
#include <QPushButton>
#include <QNetworkAccessManager>
#include <QNetworkReply>

// TODO
#include <rainfusion.h>

class tab_sources : public QWidget {
	Q_OBJECT

	struct modl {
		TaskInfo * task;
		std::string name;
		std::string path;
	};

	public:
		tab_sources(QWidget * parent = nullptr);

		bool setPrefix(prefix::Prefix current_prefix);

	public slots:
		void sourceRefreshBegin();
		void sourceRefreshRead();
		void selectedMod(QModelIndex const & index, QModelIndex const & previous_index);
		void selectedInstall();
		void download_error(QNetworkReply::NetworkError code);
		void download_progress(qint64 received, qint64 total);
		void download_finished();
		void iconDownloadRead();

	signals:
		void taskAdded(TaskInfo * info);
		void changedMods();

	private:
		rainfusion::RainfusionMod getMod(QString name);
		std::vector<rainfusion::RainfusionMod> mods;
		prefix::Prefix current_prefix;

		bool working = false;
		QNetworkAccessManager * netman;
		QListView * view_sources = nullptr;
		QPushButton * button_refresh = nullptr;
		ModView * view_mod = nullptr;
		QTreeView * view_mods = nullptr;
		QPushButton * button_install = nullptr;

		std::map<QNetworkReply *, modl> downloads;
		std::map<QNetworkReply *, rainfusion::RainfusionMod> icon_downloads;
		std::map<std::string /* uuid */, QPixmap> icons;
};

#endif //TAB_SOURCES_H
