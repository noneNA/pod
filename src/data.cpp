#include <data.h>

Data::Data() {}

template <typename T> T Data::read() {
	QByteArray b = data;
	unsigned int i = (unsigned int)position;
	T value = (T)(
		(unsigned char) b[i] |
		(unsigned char) b[i + 1] << 8 |
		(unsigned char) b[i + 2] << 16 |
		(unsigned char) b[i + 3] << 24
	);
	position += 4;
	return value;
}

std::vector<char> Data::readChars(int count) {
	std::vector<char> chars(count);
	for (int c = 0; c < count; c++) {
		chars[c] = (unsigned char)data[(unsigned int)position + c];
	}
	position += count;
	return chars;
}

unsigned char Data::readByte() {
	unsigned char read = (unsigned char)(data[(unsigned int)position]);
	position += 1;
	return read;
}

std::vector<long long> Data::setup_reader(std::string chunk) {
	if (chunk_pos.count(chunk) == 0) {
		return std::vector<long long>(0);
	}
	position = chunk_pos[chunk];
	unsigned int num = read<unsigned int>();
	std::vector<long long> array(num);
	for (unsigned int i = 0U; i < num; i++) {
		array[i] = start + read<unsigned int>();
	}
	return array;
}

std::string Data::rcs() {
	long long old_pos = position;
	while (position < end && readByte() != 0) {}
	long long num = position - old_pos - 1;
	position = old_pos;
	std::vector<char> chars = readChars((int)num);
	std::string result(chars.data(), chars.size());
	position += 1;
	return result;
}

std::string Data::rrcs() {
	long long old_pos = position;
	long long pos = start + read<unsigned int>();
	position = pos;
	std::string result = rcs();
	position = old_pos + 4;
	return result;
}

void Data::reset() {
	position = 0;
	start = 0;
	end = 0;
	chunk_pos.clear();
	lock.lock();
	progress = 0;
	completed = false;
	lock.unlock();
}

bool Data::extractResources(QString data_filename, QString modloader_path) {
	reset();

	QFile data_file(data_filename);
	if (!data_file.open(QIODevice::ReadOnly)) {
		lock.lock();
		completed = true;
		lock.unlock();
		qInfo() << "Couldn't open data file";
		return false;
	}

	data = data_file.readAll();

	while (true) {
		int val = read<int>();
		if (val == 1297239878) {
			end = read<unsigned int>();
			end += position;
			val = read<int>();
			if (val == 944653639) {
				break;
			}
		}
	}
	position -= 4L;
	start = position - 8;

	while (position < end) {
		std::vector<char> key = readChars(4);
		unsigned int num = read<unsigned int>();
		chunk_pos[std::string(key.data(), key.size())] = position;
		position += num;
	}

	position = chunk_pos["AUDO"] + 4;
	std::vector<long long> array = setup_reader("SOND");
	std::map<int, std::string> resource_filenames;
	for (unsigned int i = 0u; i < array.size(); i++) {
		std::string text = rrcs();
		position += 8L;
		position += 20L;
		int value = read<int>();
		if (value >= 0) {
			resource_filenames[value] = text;
		}
	}

	QString d_resources = modloader_path + "/resources";
	qInfo() << "Extracting resources to " << d_resources;
	if (!QDir().mkpath(d_resources)) {
		lock.lock();
		completed = true;
		lock.unlock();
		qInfo() << "Extractor couldn't make resources path";
		return false;
	}

	QString d_sound = d_resources + "/sound";
	if (!QDir().mkpath(d_sound)) {
		lock.lock();
		completed = true;
		lock.unlock();
		qInfo() << "Extractor couldn't make sound path";
		return false;
	}

	std::vector<long long> array3 = setup_reader("AUDO");
	for (unsigned int i = 0U; i < array3.size(); i++) {
		long long v = array3[i];
		position = v + 4;
		std::string extension = ((read<int>() == 1179011410) ? ".wav" : ".ogg");
		std::string write_path = resource_filenames[i] + extension;
		position = v;
		int num7 = read<int>();
		//qInfo() << QString::fromStdString(write_path);
		QFile write_file(d_sound + "/" + QString::fromStdString(write_path));
		if (!write_file.open(QIODevice::WriteOnly | QIODevice::Append))	{
			lock.lock();
			completed = true;
			lock.unlock();
			return false;
		}
		QDataStream out(&write_file);
		while (--num7 >= 0) {
			out << readByte();
		}
		write_file.close();
		lock.lock();
		progress += 1;
		lock.unlock();
	}

	/*
	//QString d_music = d_resources + "/music";
	QString d_music = d_resources + "/sound";
	if (!QDir().mkpath(d_music)) {
		lock.lock();
		completed = true;
		lock.unlock();
		return false;
	}
	*/

	QDirIterator ogg(QFileInfo(data_filename).absolutePath(), QStringList() << "*.ogg", QDir::Files);
	while (ogg.hasNext()) {
		QString filename = ogg.next();
		QFile file(filename);
		QFileInfo info(filename);
		QString basename = info.fileName();
		//file.copy((basename == "wChefShoot2_1.ogg" ? d_sound : d_music) + "/" + basename);
		file.copy(d_sound + "/" + basename);
		lock.lock();
		progress += 1;
		lock.unlock();
	}

	lock.lock();
	completed = true;
	lock.unlock();
	return true;
}
