#include <runner_info.h>

#include <QtGlobal>
#include <QMessageLogger>
#include <QtDebug>
#include <QDir>
#include <QDirIterator>

std::vector<runner_info> runner_info::getRunners() {
	std::vector<runner_info> all_runners {};

	#if defined(__WIN32)
		runners.push_back(runner_info {
			.type = runner_type::Native,
			.short_name = "Native",
			.path = "",
		});
	#else /* __linux__ */ /* __APPLE__ */
		auto wine_runners = runner_info::getWineRunners();
		all_runners.insert(all_runners.end(), wine_runners.begin(), wine_runners.end());

		auto proton_runners = runner_info::getProtonRunners();
		all_runners.insert(all_runners.end(), proton_runners.begin(), proton_runners.end());
	#endif

	if (all_runners.empty()) {
		all_runners.push_back(runner_info {
			.type = runner_type::Missing,
			.short_name = "[ERROR] no wine or proton runner found",
			.path = "",
		});
	}

	return all_runners;
}

std::vector<runner_info> runner_info::getWineRunners() {
	std::vector<runner_info> runners {};

	#if defined(__WIN32)
		// TODO
	#elif defined(__linux__)
		auto path = QString::fromLocal8Bit(qgetenv("PATH"));
		for (auto const & path : path.split(':')) {
			QDirIterator path_items(path, QDir::Files | QDir::Executable);
			while (path_items.hasNext()) {
				auto item = QFileInfo(path_items.next());

				if (item.fileName() != "wine") {
					continue;
				}

				qInfo() << "Found wine install" << item.absoluteFilePath();

				runners.push_back(runner_info {
					.type = runner_type::Wine,
					.short_name = "Wine",
					.path = item.absoluteFilePath().toStdString(),
				});
			}
		};
	#elif defined(__APPLE__)
		// TODO
	#endif

	return runners;
}

std::vector<runner_info> runner_info::getProtonRunners() {
	std::vector<runner_info> runners {};

	#if defined(__WIN32)
		// TODO
	#elif defined(__linux__)
		auto steam_common = QDir::home();
		steam_common.cd(".local/share/Steam/steamapps/common");

		auto valid_proton_dir_regex = QRegExp(R"(Proton \d+\.\d+)");
		QDirIterator dirs(steam_common.path(), QDir::Dirs);
		while (dirs.hasNext()) {
			auto dir = QDir(dirs.next());
			QString dir_name = dir.dirName();

			bool valid_dir = dir_name == "Proton - Experimental"
				|| valid_proton_dir_regex.exactMatch(dir_name);

			bool has_proton = dir.exists("proton");

			if (!valid_dir || !has_proton) {
				continue;
			}

			qInfo() << "Found proton install" << dir.absolutePath();

			runners.push_back(runner_info {
				.type = runner_type::Proton,
				.short_name = dir_name.toStdString(),
				.path = dir.absoluteFilePath("proton").toStdString(),
			});
		}

	#elif defined(__APPLE__)
		// TODO
	#endif

	return runners;
}
