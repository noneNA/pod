#include <tab_home.h>

// TODO
#include <QDebug>

#include <string>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QClipboard>
#include <QGuiApplication>

tab_home::tab_home() {
	QHBoxLayout * layout_main = new QHBoxLayout(this);

	QVBoxLayout * layout_modlist = new QVBoxLayout;

	label_modlist = new QLabel("Modlist");
	layout_modlist->addWidget(label_modlist);

	text_modlist = new QTextEdit();
	text_modlist->setReadOnly(true);
	layout_modlist->addWidget(text_modlist);

	button_copy = new QPushButton("Copy");
	connect(button_copy, &QPushButton::clicked, this, &tab_home::clickedCopy);
	layout_modlist->addWidget(button_copy);

	// TODO Remove
	button_refresh = new QPushButton("Refresh");
	connect(button_refresh, &QPushButton::clicked, this, &tab_home::clickedRefresh);
	layout_modlist->addWidget(button_refresh);

	layout_main->addLayout(layout_modlist);
}

void tab_home::changedCurrentProfile(profile::ProfileFile current_profile) {
	this->current_profile = current_profile;
	refresh();
}

bool tab_home::refresh() {
	current_profile.read();
	if (!refreshModlist())
		return false;
	return true;
}

bool tab_home::refreshModlist() {
	text_modlist->clear();
	for (std::string mod_name : current_profile.data.enabled_mods) {
		text_modlist->insertPlainText(QString::fromStdString(mod_name + "\n"));
	}
	return true;
}

void tab_home::clickedCopy() {
	refresh();
	QClipboard * clipboard = QGuiApplication::clipboard();
	clipboard->setText(text_modlist->toPlainText());
}

// TODO Remove
void tab_home::clickedRefresh() {
	qInfo() << "Refreshing home tab";
	refresh();
}
