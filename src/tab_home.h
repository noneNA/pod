#ifndef TAB_HOME_H
#define TAB_HOME_H

#include <profile.h>

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QTextEdit>

class tab_home : public QWidget {
	Q_OBJECT

	public:
		tab_home();

		bool refresh();

	private:
		QLabel * label_modlist = nullptr;
		QTextEdit * text_modlist = nullptr;
		QPushButton * button_copy = nullptr;
		profile::ProfileFile current_profile;

		// TODO Remove
		QPushButton * button_refresh = nullptr;

		bool refreshModlist();

	public slots:
		void changedCurrentProfile(profile::ProfileFile current_profile);
		void clickedCopy();

		// TODO Remove
		void clickedRefresh();
};

#endif //TAB_HOME_H
