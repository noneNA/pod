#ifndef DATA_H
#define DATA_H

#include <QtCore>

#include <string>
#include <map>
#include <mutex>


static const int FILE_COUNT = 173;

class Data {
	public:
		Data();
		bool extractResources(QString data_filename, QString modloader_path);

	public:
		std::mutex lock;
		bool completed = false;
		int progress = 0;

	private:
		template <typename T> T read();
		std::vector<char> readChars(int count);
		unsigned char readByte();
		std::vector<long long> setup_reader(std::string chunk);
		std::string rcs();
		std::string rrcs();
		void reset();

	private:
		QByteArray data;
		long long position = 0;
		long long start = 0;
		long long end = 0;
		std::map<std::string, long long> chunk_pos;
};

#endif //DATA_H
