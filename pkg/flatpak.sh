#!/bin/sh

#flatpak-builder --user --install --force-clean build-dir io.github.none_na.Pod.yml

_id="io.github.none_na.Pod"
_manifest="./assets/$_id.yml"
flatpak-builder --repo=repo --force-clean build-dir "$_manifest"
flatpak remote-add --if-not-exists --no-gpg-verify pod-repo repo
flatpak install --or-update pod-repo "$_id"
