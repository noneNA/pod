# Pod 0.2.2
* Temporarily migrate to new rainfusion mirror
* Fix rainfusion downloads having wrong directory names

# Pod 0.2.1
* Build system improvements
* Bug fixes

# Pod 0.2.0
* Basic Rainfusion API support including icons
* Home tab for copying current mods
* Button to create new blank save
* Fixed saves not being imported
* Bug fixes and some minor improvements

# Pod 0.1.2
* Prefix management
* Logs tab for monitoring game output
* Launched process count is tracked
* Vanilla setter (Needs restart for now)
* Window focus changes
* Creating prefixes
* Installing multiple mods from one zip
* Fixed setup not extracting resources
* Basic handling of abnormal input

# Pod 0.1.1
* Basic savefile importing
* Updating modloader
* Tweaked setup
* Quazip is now statically compiled
* Fixed some mods not downloading on windows

# Pod 0.1.0
* Initial release
