{
  description = "Nix flake for Pod";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
  flake-utils.lib.eachDefaultSystem (system:
  let pkgs = nixpkgs.legacyPackages.${system}; in
  {
    packages = rec {

      default = generic.overrideAttrs (prev: {
        name = "pod";
        nativeBuildInputs = prev.nativeBuildInputs ++ [ pkgs.libsForQt5.wrapQtAppsHook ];
        dontWrapQtApps = false;
        fixupPhase = ''
        '';
      });

      generic = with pkgs; pkgs.makeOverridable stdenv.mkDerivation {
        name = "pod_generic";
        src = self;
        buildInputs = [ qt5.qtbase ];
        nativeBuildInputs = [ cmake ];
        dontWrapQtApps = true;
        fixupPhase = ''
          patchelf --set-interpreter /lib64/ld-linux-x86-64.so.2 "$out"/bin/pod
        '';
      };

    };
  });
}
